package ef.codeyourown.baseService.auth.controller;

import ef.codeyourown.baseService.auth.models.AuthRequest;
import ef.codeyourown.baseService.auth.models.AuthResponse;
import ef.codeyourown.baseService.auth.services.AuthService;
import org.springframework.web.bind.annotation.*;

import javax.persistence.EntityNotFoundException;

@RestController
@RequestMapping("/authenticate")
@CrossOrigin
public class AuthController {
    private final AuthService authService;

    public AuthController(AuthService authService) {
        this.authService = authService;
    }

    @PostMapping("/login")
    public AuthResponse login(@RequestBody final AuthRequest authRequest) {
        return this.authService.generateToken(authRequest);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    public AuthResponse userNotFound() {
        return new AuthResponse("");
    }
}
