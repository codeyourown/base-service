package ef.codeyourown.baseService.auth.services;

import ef.codeyourown.baseService.auth.models.AuthRequest;
import ef.codeyourown.baseService.auth.models.AuthResponse;
import ef.codeyourown.baseService.user.services.UserRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;

/**
 * Authentication Service to be used in Controller for login
 */
@Service
public class AuthService {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;
    private final JwtService jwtService;

    public AuthService(UserRepository userRepository, JwtService jwtService, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.jwtService = jwtService;
        this.passwordEncoder = passwordEncoder;
    }

    public AuthResponse generateToken(AuthRequest authRequest) throws EntityNotFoundException {
        return userRepository.findFirstByMail(authRequest.getEmail())
                .filter(user -> passwordEncoder.matches(authRequest.getPassword(), user.getPassword()))
                .map(user -> new AuthResponse(jwtService.generateToken(authRequest.getEmail())))
                .orElseThrow(() -> new EntityNotFoundException("User not found!"));
    }
}
