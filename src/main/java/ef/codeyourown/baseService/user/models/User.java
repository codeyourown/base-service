package ef.codeyourown.baseService.user.models;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.util.Collection;

/**
 * UserDTO to be generated from UserEntity class
 * Implementing UserDetails for SpringSecurity Filter
 * Mail is used as username, as it is a real unique identifier
 */
public class User implements UserDetails {

    String name;
    String surname;
    String mail;
    UserRole userRole;
    String password;

    public User() {
    }

    /**
     * Create a User-Object from an UserEntity
     * @param userEntity object from database o.s.
     */
    public User(UserEntity userEntity) {
        this.name = userEntity.getName();
        this.surname = userEntity.getSurname();
        this.mail = userEntity.getEmail();
        this.userRole = userEntity.getUserRole();
        this.password = userEntity.getPassword();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public String getPassword() {
        return this.password;
    }

    @Override
    public String getUsername() {
        return this.mail;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public UserRole getUserRole() {
        return userRole;
    }

    public void setUserRole(UserRole userRole) {
        this.userRole = userRole;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
