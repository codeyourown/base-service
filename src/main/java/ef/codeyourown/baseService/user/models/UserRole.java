package ef.codeyourown.baseService.user.models;

public enum UserRole {
    Standard,
    Controller,
    Admin
}
