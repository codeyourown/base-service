package ef.codeyourown.baseService.user.services;

import ef.codeyourown.baseService.user.models.UserEntity;
import ef.codeyourown.baseService.user.models.UserRole;
import org.springframework.boot.context.event.ApplicationStartedEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserStartUpService implements ApplicationListener<ApplicationStartedEvent> {
    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public UserStartUpService(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void onApplicationEvent(ApplicationStartedEvent applicationStartedEvent) {
//        if (this.userRepository.count() > 0) {
//            System.out.println("Already users there");
//            return;
//        }
        UserEntity user = new UserEntity();
        user.setEmail("testmail@test.com");
        user.setUserRole(UserRole.Admin);
        user.setSurname("SurTest");
        user.setName("Test");
        user.setPassword("pass");
        this.userRepository.save(user);
    }
}
