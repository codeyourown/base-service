package ef.codeyourown.baseService.user.services;

import ef.codeyourown.baseService.user.models.User;
import ef.codeyourown.baseService.user.models.UserEntity;

import java.util.ArrayList;
import java.util.List;

public class UserConverter {
    public static User toUser(UserEntity userEntity) {
        return new User(userEntity);
    }

    public static UserEntity toUserEntity(User user) {
        UserEntity userEntity = new UserEntity();
        userEntity.setName(user.getName());
        userEntity.setSurname(user.getSurname());
        userEntity.setUserRole(user.getUserRole());
        userEntity.setEmail(user.getMail());
        return userEntity;
    }

    public static List<User> toUserEntity(Iterable<UserEntity> userEntities) {
        List<User> users = new ArrayList<>();
        userEntities.forEach(userEntity -> users.add(toUser(userEntity)));
        return users;
    }
}
