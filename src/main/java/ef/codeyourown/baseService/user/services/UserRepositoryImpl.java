package ef.codeyourown.baseService.user.services;

import ef.codeyourown.baseService.user.models.UserEntity;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Optional;
import java.util.List;

@Service
public class UserRepositoryImpl implements UserRepository {
    private final RestTemplate restTemplate;

    public UserRepositoryImpl(RestTemplateBuilder restTemplateBuilder) {
        this.restTemplate = restTemplateBuilder.build();
    }

    @Override
    public Optional<UserEntity> findFirstByMail(String mail) {
        final String url = "http://localhost:8090/api/user/?mail="+mail;
        final UserEntity user = this.restTemplate.getForObject(url, UserEntity.class);
        if (user == null || user.getEmail() == null || user.getEmail().equals("")) {
            return Optional.empty();
        }
        return Optional.of(user);
    }

    @Override
    public UserEntity save(UserEntity userEntity) {
        final String url = "http://localhost:8090/api/user";
        final HttpEntity<UserEntity> request = new HttpEntity<>(userEntity);
        return this.restTemplate.postForObject(url, request, UserEntity.class);
    }

    @Override
    public Iterable<UserEntity> saveAll(Iterable<UserEntity> userEntities) {
        List<UserEntity> result = new ArrayList<>();
        userEntities.forEach(s -> {
            result.add(this.save(s));
        });
        return result;
    }

    @Override
    public Iterable<UserEntity> findAll() {
        final String url = "http://localhost:8090/api/user/all";
        UserEntity[] usersArray = this.restTemplate.getForObject(url, UserEntity[].class);
        assert usersArray != null;
        return Arrays.asList(usersArray);
    }
}
