package ef.codeyourown.baseService.user.services;

import ef.codeyourown.baseService.user.models.UserEntity;

import java.util.Optional;

public interface UserRepository {
    Optional<UserEntity> findFirstByMail(String mail);
    Iterable<UserEntity> findAll();
    UserEntity save(UserEntity user);
    Iterable<UserEntity> saveAll(Iterable<UserEntity> userEntities);
}
