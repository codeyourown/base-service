package ef.codeyourown.baseService.user.controller;

import ef.codeyourown.baseService.user.models.UserEntity;
import ef.codeyourown.baseService.user.services.UserRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserController {
    private final UserRepository userRepository;

    public UserController(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @PostMapping("/create")
    public UserEntity createUser(@RequestBody final UserEntity user) {
        final UserEntity createdUser = this.userRepository.save(user);
        return createdUser;
    }

    @GetMapping("/all")
    public List<UserEntity> getAllUsers() {
        return StreamSupport
                .stream(this.userRepository.findAll().spliterator(), false)
                .collect(Collectors.toList());
    }
}
